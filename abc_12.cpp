﻿
//1. В main, используя цикл и условия, вывести все четные числа от 0 до N (N задавать константой в начале программы).
//2. Написать функцию, которая в зависимости от своих параметров печатает в консоль либо четные, 
//либо нечетные числа от 0 до N(N тоже сделать параметром функции).
//Минимизировать количество циклов и условий.
#include <iostream>
using namespace std;

void func_example(int S, int R)
{
	for (R; R < S; R += 2)
		cout << R << endl;
}

int main()
{
	//Первое задание
	int A, B = 0;											
	cout << "Enter Number > \n";						
	cin >> A;									
	cout << "Even Number: \n";
	func_example(A, B);
	cout << "\n";

	//Второе задание
	cout << "Enter Number > ";
	int Z;
	cin >> Z;
	cout << "Enter 1 or 0 > ";
	bool PRND;
	cin >> PRND;
	int X = PRND;
	func_example(Z, X);
}

